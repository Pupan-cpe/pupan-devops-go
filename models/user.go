package models

import (
	"time"

	"github.com/matthewhartstonge/argon2"
	"gorm.io/gorm"
)

// ทำให้ value เป็นตัวเล็ก ในรูปแบบ json
type User struct {
	ID        uint  `json:"id" gorm:"primaryKey;autoIncrement;not null"`
	FullName  string `json:"full_name" gorm:"type:varchar(255);not null"`
	Email     string `json:"email" gorm:"type:varchar(255);not null;unique"`
	Password  string `json:"-" gorm:"type:varchar(255);not null"` //q  ignore pass = "-"
	IsAdmin   bool `json:"is_admin" gorm:"type:bool;default:false"` //? is_active ถ้ามีcol ในdb แล้วใช้ column:is_active มันจะแบบ is_admin เข้ากับ is_active
	CreatedAt time.Time `json:"created_at"` 
	UpdatedAt time.Time `json:"update_at"` 
}

// ----------------- hash password --------------------------------
func (user *User)BeforeCreate(db *gorm.DB) error {

user.Password = hashPassword(user.Password)

return nil

}


func hashPassword(Password string) (string) {

argon := argon2.DefaultConfig()

encoded, _ := argon.HashEncoded([]byte(Password))
return string(encoded)
}
// ---------------- end of hash password------------------------------



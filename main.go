package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/Pupan-cpe/learn-go/configs"
	"gitlab.com/Pupan-cpe/learn-go/routes"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	router := SetupRoutes()
	router.Run(":"+os.Getenv("GO_PORT"))
}


func SetupRoutes()*gin.Engine {
	//q load .env
	godotenv.Load(".env")
	// connect DB 
	configs.ConnectDB()

	router:= gin.Default()

	apiv1 :=router.Group("/api/v1")
	home:=router.Group("/")

	routes.InitHomeRoutes(home)

	routes.InitUserRoutes(apiv1)

	

	return router


}
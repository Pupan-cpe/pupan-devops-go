FROM golang:1.16.5 as builder

WORKDIR /app
 
COPY . .

RUN CGO_ENABLED=0 go build -o goapp main.go

FROM alpine:latest

WORKDIR /app
COPY --from=builder /app/goapp ./goapp

EXPOSE 5000

CMD ["./goapp"]
package configs

import (
	"fmt"

	"gitlab.com/Pupan-cpe/learn-go/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)
var DB *gorm.DB

func ConnectDB() {


	DATABASE := "host=203.150.221.207 user=root password=password dbname=goDB port=5432 sslmode=disable TimeZone=Asia/Bangkok"
	// envDB := os.Getenv("DATABASE")
	db, err := gorm.Open(postgres.Open(DATABASE), &gorm.Config{})





	
	// db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err!=nil {
		
		fmt.Println("ไม่สามารถติดต่อกับฐานข้อมูลได้")
		
		fmt.Println(err.Error())
		panic(err)

	}else{
		// DB = db
		DB = db

		fmt.Println("Connect DB success!")

		// migration

		// db.Migrator().DropTable(&models.User{})  //?Drop Table

		db.AutoMigrate(&models.User{})

	}
}
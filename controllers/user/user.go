package usercontroller

import (
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"github.com/matthewhartstonge/argon2"
	"gitlab.com/Pupan-cpe/learn-go/configs"
	"gitlab.com/Pupan-cpe/learn-go/models"
	"gitlab.com/Pupan-cpe/learn-go/utils"
)


func GetAll(c *gin.Context) {

	var users  []models.User




	// configs.DB.Select("id, full_name,email").Find(&users)  //? get All Table users --->  select  local
	configs.DB.Find(&users)
	// configs.DB.Order("id DESC").Find(&users) // select all table ORDER BY
	//configs.DB.Raw("select * from users order by id desc").Scan(&users) // sql query  


	c.JSON(200, gin.H{
		"data": users,
		
	})
}

func Register(c *gin.Context) {
	var input InputUser
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		user := models.User{

			FullName: input.FullName,
			Email: input.Email,
			Password: input.Password,
			
		}
// check email duplicate 

	userExists := configs.DB.Where("email = ?", input.Email).First(&user)

	if userExists.RowsAffected == 1 {

		c.JSON(http.StatusBadRequest, gin.H{"error": "duplicate Email"})
		return

	}
	result :=	configs.DB.Debug().Create(&user)


	// fmt.Println(result)
		if result.Error != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": result.Error})
			return
		}else{
		c.JSON(201, gin.H{
			"message": "success",
			})
		}
	
}


func Login(c *gin.Context) {

	

	var input InputLogin
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := models.User{

		Email: input.Email,
		Password: input.Password,
		
	}

// check email
	userAccount := configs.DB.Where("email = ?", input.Email).First(&user)

	if userAccount.RowsAffected == 0 {

		c.JSON(http.StatusBadRequest, gin.H{"error": "User not found"})
		return

	}

//  check Password duplicate

	ok,_  := argon2.VerifyEncoded([]byte(input.Password), []byte(user.Password))  // convert str to byte   input = client   user = db query 


	// fmt.Println(result)
	if !ok{
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Password not found",})
		return

	}


	//  created Token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{

		"user_id": user.ID,
		"host": "this is token Pupan",
		"exp": time.Now().Add(time.Minute*20).Unix(),
		

	})

	jwtSecret := os.Getenv("JWT_SECRET")
	accessToken, _ := token.SignedString([]byte(jwtSecret))

	
		c.JSON(201, gin.H{
			"message": "Login success",
			"access_token": accessToken,
			})
	
}

func GetbyId(c *gin.Context) {
	
	id:= c.Param("id")

	var user  models.User
	result := configs.DB.First(&user, id)

	if(result.RowsAffected < 1 ){

		c.JSON(404, gin.H{
			"error": "user not found",
			})	
	}else{

		
	c.JSON(200, gin.H{
		"data": user,
		})	

	}
}

func SearchByFullname(c *gin.Context) {

	
	

	fullname:= c.Query("fullname")
	var users []models.User
	result := configs.DB.Where("full_name LIKE ?", "%" +fullname+ "%").Scopes(utils.Paginate(c)).Find(&users)

	// total := configs.DB.Select("id").Row().Scan(&users)

    // total := configs.DB.Raw("SELECT COUNT(id)FROM users;").Scan(&users) // sql query  


	if result.RowsAffected < 1 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "not found"})
		return
	}

	c.JSON(200, gin.H{
	"data": users,
	// "total": total,
	})
}


func GetProfile(c *gin.Context) {

	user := c.MustGet("user")  //q Get User decode token Global

	c.JSON(200, gin.H{
	"data": user,
	// "total": total,
	})
}

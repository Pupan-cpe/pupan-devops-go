package middlewares

import (
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"gitlab.com/Pupan-cpe/learn-go/configs"
	"gitlab.com/Pupan-cpe/learn-go/models"
)

func AuthJWT() gin.HandlerFunc {

	return gin.HandlerFunc(func(c *gin.Context) {

		if c.GetHeader("Authorization") == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
			defer c.AbortWithStatus(http.StatusUnauthorized) //? defer บรรทัดนี้ ให้ ทำ เป็นการหน่วงเวลา ให้ทำสุดท้ายเสมอ  
			//*  AbortWithStatus
		}

		tokenHeader := c.GetHeader("Authorization")
		accessToken := strings.SplitAfter(tokenHeader, "Bearer")[1] // split  bearer  ออก 
		// fmt.Println(accessToken)
		jwtSecretKey := os.Getenv("JWT_SECRET")

		token, _ := jwt.Parse(strings.Trim(accessToken, " "), func(token *jwt.Token) (interface{}, error) { // jwt.Parse  เอาไว้ check ว่า ใช่ token ของเราจริงไหม
			return []byte(jwtSecretKey), nil
		})

		if !token.Valid {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
			defer c.AbortWithStatus(http.StatusUnauthorized)
		} else {
			//q global value result
			claims := token.Claims.(jwt.MapClaims)
			var user models.User
			configs.DB.First(&user, claims["user_id"])
			c.Set("user", user) //? c.Set gobal varibles
			//q return to next method if token is exist
			c.Next()  
		}

	})
}
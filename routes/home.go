package routes

import (
	"github.com/gin-gonic/gin"
	homeController "gitlab.com/Pupan-cpe/learn-go/controllers/home"
)

func InitHomeRoutes(rg *gin.RouterGroup) {

	routerGroup := rg

	routerGroup.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"API VERSION": "1.0.0",
		})
	})
	//? routes

	routerGroup.POST("/home", homeController.PostData)
	// routerGroup.POST("/home/:id", homeController.GetID)
	// routerGroup.GET("/home/name", homeController.QueryString)

	

}
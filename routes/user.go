package routes

import (
	"github.com/gin-gonic/gin"
	usercontroller "gitlab.com/Pupan-cpe/learn-go/controllers/user"
	"gitlab.com/Pupan-cpe/learn-go/middlewares"
)

func InitUserRoutes(rg *gin.RouterGroup) {
	routerGroup := rg.Group("users")

	//routerGroup := rg.Group("users").Use(middlewares.AuthJWT())  //? ทุกอันที่อยู่ใน group จะต้อง login ทั้งหมด 


	routerGroup.GET("/", usercontroller.GetAll)
	routerGroup.POST("/register",usercontroller.Register)
	routerGroup.POST("/login",usercontroller.Login)
	routerGroup.GET("/:id",usercontroller.GetbyId)
	routerGroup.GET("/search",usercontroller.SearchByFullname)


	//get Profile 
	routerGroup.GET("/profile",middlewares.AuthJWT(),usercontroller.GetProfile)  //? ถ้าเป็นแบบ route จะเอาไว้ตรงกลาง 



}